#include "AJInit.h"

#if defined(ALLJOYN_VERSION) && ALLJOYN_VERSION >= 1504
#include <alljoyn/Init.h>
#endif

#include <alljoyn/Status.h>

namespace twobulls
{

AJInitGuard::AJInitGuard()
{
    QStatus status = ER_OK;
#if defined(ALLJOYN_VERSION) && ALLJOYN_VERSION >= 1504
    status = AllJoynInit();
#if defined(ALLJOYN_BUNDLED_ROUTER)
    if (ER_OK == status)
    {
        status = AllJoynRouterInit();
    }
#endif // defined(ALLJOYN_BUNDLED_ROUTER)
#endif // defined(ALLJOYN_VERSION) && ALLJOYN_VERSION >= 1504
    inited_ = status == ER_OK;
}

AJInitGuard::~AJInitGuard()
{
    QStatus status = ER_OK;
#if defined(ALLJOYN_VERSION) && ALLJOYN_VERSION >= 1504
#if defined(ALLJOYN_BUNDLED_ROUTER)
    status = AllJoynRouterShutdown();
#endif // defined(ALLJOYN_BUNDLED_ROUTER)
    status = AllJoynShutdown();
#endif // defined(ALLJOYN_VERSION) && ALLJOYN_VERSION >= 1504
}

bool AJInitGuard::isInited()
{
    return inited_;
}
    
} // namespace twobulls
