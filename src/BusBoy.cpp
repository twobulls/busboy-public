// Copyright 2015 Two Bulls Holding Pty Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "BusBoy.h"

#include <cstdio>

#include <qcc/GUID.h>
#include <alljoyn/Status.h>
#include <alljoyn/AboutIconObj.h>
#include <alljoyn/AboutObj.h>
#include <alljoyn/BusAttachment.h>

#include "tinyxml2.h"

#if defined(ENABLE_BUSBOY_LOGGING)
#define STRINGIZE(s) #s
#define XSTRINGIZE(s) STRINGIZE(s)
#define BUSBOYLOG(fmt, ...) do { \
    FILE* fp = stdout; \
    fprintf(fp, __FILE__ "(" XSTRINGIZE(__LINE__) ") " fmt "\n", ##__VA_ARGS__); \
    fflush(fp); \
} while (0);
#else
#define BUSBOYLOG(fmt, ...) do {} while(0);
#endif

#define AJ_RESULT(result_var, statement) \
    { \
        QStatus status = statement; \
        result_var = ER_OK == status; \
        if (!result_var) { \
            BUSBOYLOG(XSTRINGIZE(statement) " <- %s", QCC_StatusText(status)); \
        } \
    }

namespace twobulls {

const char *SECURE_AUTH_MECH = "ALLJOYN_ECDHE_NULL ALLJOYN_ECDHE_ECDSA";

BusBoy::BusBoy(const std::string &aboutXML,
			   const std::string &interfaceXML,
               const ajn::SessionPort port,
               const std::vector<unsigned char> &iconData,
               bool secure,
               ajn::KeyStoreListener* keyStoreListener) :
    ajn::BusObject(ExtractPathName(interfaceXML).c_str()), mAboutXML(aboutXML), mInterfaceXML(interfaceXML), mInterfaceName(ExtractInterfaceName(interfaceXML)), mAboutIconData(iconData), mBusAttachment(NULL),
    mAboutData(NULL), mAboutObject(NULL), mAboutIcon(NULL), mAboutIconObject(NULL),
    mInterface(NULL), mApplicationName(), mLanguage(), mSessionPort(port), mSecure(secure), mKeyStoreListener(keyStoreListener) {
    BUSBOYLOG("::BusBoy -> ");

    DigestAboutXML();

    BUSBOYLOG("::BusBoy <-");
}

BusBoy::~BusBoy() {
    BUSBOYLOG("::~BusBoy -> ");

    Stop();

    if (mKeyStoreListener != NULL) {
        delete mKeyStoreListener;
        mKeyStoreListener = NULL;
    }

    BUSBOYLOG("::~BusBoy <-");
}

bool BusBoy::Start() {
    BUSBOYLOG("::Start -> ");

    bool result = mAJiniter.isInited();

    if (result) {
        result = SetupBusAttachment();
        BUSBOYLOG("::Start -- SetupBusAttachment <- %d", result);
    }

    if (result) {
        result = SetupAboutObject();
        BUSBOYLOG("::Start -- SetupAboutObject <- %d", result);
    }

    BUSBOYLOG("::Start <- %d", result);

    return result;
}

void BusBoy::Stop() {
    BUSBOYLOG("::Stop -> ");

    if (mAboutObject != NULL) {
        mAboutObject->Unannounce(); // This seems to reduce the chance of hanging in mBusAttachment->Disconnect()
    }

    if (mBusAttachment != NULL) {
        mBusAttachment->UnbindSessionPort(mSessionPort);
        mBusAttachment->UnregisterBusObject(*this);
        mBusAttachment->EnablePeerSecurity("",
                                           NULL,
                                           NULL,
                                           true); // remove auth listener before bus attachment is destructed
        mBusAttachment->UnregisterKeyStoreListener(); // NOTE: deletes mKeyStoreListener
        mKeyStoreListener = NULL;
        mBusAttachment->Disconnect(); // TODO: this can hang, possibly related to? https://jira.allseenalliance.org/browse/ASACORE-1542
        BUSBOYLOG("::BusAttachment->Stop : %s", mBusAttachment->GetUniqueName().c_str());
        mBusAttachment->Stop();
        mBusAttachment->Join();
    }

    if (mAboutIconObject != NULL) {
        delete mAboutIconObject;
        mAboutIconObject = NULL;
    }

    if (mAboutIcon != NULL) {
        delete mAboutIcon;
        mAboutIcon = NULL;
    }

    if (mAboutObject != NULL) {
        delete mAboutObject;
        mAboutObject = NULL;
    }

    if (mAboutData != NULL) {
        delete mAboutData;
        mAboutData = NULL;
    }

#if defined(ENABLE_BUSBOY_BOOST_SHAREDPTR)
    mBusAttachment.reset();
#else
    if(mBusAttachment != NULL) {
    delete mBusAttachment;
    mBusAttachment = NULL;
}
#endif

    BUSBOYLOG("::Stop <-");
}

bool BusBoy::TriggerEvent(const std::string &eventName) {
    BUSBOYLOG("::TriggerEvent -> eventName = %s", eventName.c_str());

    bool result = mInterface != NULL;
    if (result) {
        const ajn::InterfaceDescription::Member *eventSignal = mInterface->GetSignal(eventName.c_str());
        result = eventSignal != NULL;
        BUSBOYLOG("::TriggerEvent -- mInterface->GetSignal <- %d", result);

        if (result) {
            AJ_RESULT(result, Signal(NULL, 0, *eventSignal, NULL, 0, 0, ajn::ALLJOYN_FLAG_SESSIONLESS));
            BUSBOYLOG("::TriggerEvent -- Signal <- %d", result);
        }
    }

    BUSBOYLOG("::TriggerEvent <- %d", result);

    return result;
}

void BusBoy::DoAction(const ajn::InterfaceDescription::Member* member, ajn::Message& message) {
	// Customize
}

bool BusBoy::GetProperty(const std::string &propertyName, ajn::MsgArg &outPropertyValue) {
	// Customize
	return false;
}

bool BusBoy::SetProperty(const std::string &propertyName, const ajn::MsgArg &inPropertyValue) {
	// Customize
	return false;
}

std::string BusBoy::ExtractInterfaceName(const std::string &interfaceXML) {
    tinyxml2::XMLDocument xmlDoc;

    std::string interfaceName;

    bool result = xmlDoc.Parse(interfaceXML.c_str()) == tinyxml2::XML_SUCCESS;
    BUSBOYLOG("::DigestInterfaceXML -- xmlDoc.Parse <- %d", result);

    if (result) {
        const tinyxml2::XMLElement *root = xmlDoc.RootElement();
        result = root != NULL;
        BUSBOYLOG("::DigestInterfaceXML -- xmlDoc.RootElement <- %d", result);

        if (result) {
            result = std::string(root->Name()) == "interface";
            BUSBOYLOG("::DigestInterfaceXML -- root->Name == 'interface' <- %d", result);
        }

        if (result) {
            interfaceName = root->FindAttribute("name")->Value();
            result = interfaceName.length() > 0;
            BUSBOYLOG("::DigestInterfaceXML -- mInterfaceName.length <- %d", result);
        }
    }

    return interfaceName;
}

std::string BusBoy::ExtractPathName(const std::string &interfaceXML) {
    std::string interfaceName = ExtractInterfaceName(interfaceXML);
    std::string pathName(std::string("/") + interfaceName);
    std::replace(pathName.begin(), pathName.end(), '.', '/' );
    return pathName;
}

bool BusBoy::DigestAboutXML() {
    BUSBOYLOG("::DigestAboutXML -> ");

    tinyxml2::XMLDocument xmlDoc;

    mApplicationName.clear();
    mLanguage.clear();

    bool result = xmlDoc.Parse(mAboutXML.c_str()) == tinyxml2::XML_SUCCESS;
    BUSBOYLOG("::DigestAboutXML -- xmlDoc.Parse <- %d", result);

    if (result) {
        const tinyxml2::XMLElement *root = xmlDoc.RootElement();
        result = root != NULL;
        BUSBOYLOG("::DigestAboutXML -- xmlDoc.RootElement <- %d", result);

        if (result) {
            result = std::string(root->Name()) == "About";
            BUSBOYLOG("::DigestAboutXML -- root->Name == 'About' <- %d", result);
        }

        if (result) {
            for (const tinyxml2::XMLElement *child = root->FirstChildElement(); child != NULL;
                 child = child->NextSiblingElement()) {
                const std::string childName(child->Name());
                if (childName == "AppName") {
                    mApplicationName = child->GetText();
                } else if (childName == "DefaultLanguage") {
                    mLanguage = child->GetText();
                }
            }
            result = mApplicationName.length() > 0 && mLanguage.length() > 0;
            BUSBOYLOG("::DigestAboutXML -- mApplicationName.length && mLanguage.length <- %d", result);
        }
    }

    BUSBOYLOG("::DigestAboutXML <- %d", result);

    return result;
}

bool BusBoy::SetupBusAttachment() {
    BUSBOYLOG("::SetupBusAttachment -> ");

    bool result = mApplicationName.length() > 0 && mBusAttachment == NULL;
    BUSBOYLOG("::SetupBusAttachment -- mApplicationName.length && mBusAttachment <- %d", result);

    if (result) {
#if defined(ENABLE_BUSBOY_BOOST_SHAREDPTR)
        mBusAttachment = BusAttachmentPtr(new ajn::BusAttachment(mApplicationName.c_str(), true));
        result = bool(mBusAttachment);
#else
        mBusAttachment = new ajn::BusAttachment(mApplicationName.c_str(), true);
        result = mBusAttachment!= NULL;
#endif
        BUSBOYLOG("::SetupBusAttachment -- new ajn::BusAttachment <- %d", result);
    }

    if (result) {
        AJ_RESULT(result, mBusAttachment->Start());
        BUSBOYLOG("::SetupBusAttachment -- mBusAttachment->Start <- %d", result);
    }

    if (result) {
        result = DefineInterface();
        BUSBOYLOG("::SetupBusAttachment -- DefineInterface <- %d", result);
    }

    if (result) {
        result = AttachInterface();
        BUSBOYLOG("::SetupBusAttachment -- AttachInterface <- %d", result);
    }

    if (result) {
        AJ_RESULT(result, mBusAttachment->Connect());
        BUSBOYLOG("::SetupBusAttachment -- mBusAttachment->Connect <- %d", result);
        BUSBOYLOG("::BusAttachment : %s", mBusAttachment->GetUniqueName().c_str());
    }

    if (result && mSecure) {
        if (mKeyStoreListener) {
            mBusAttachment->RegisterKeyStoreListener(*mKeyStoreListener);
        }

        ajn::PermissionConfigurationListener *pcl = this;

        AJ_RESULT(result, mBusAttachment->EnablePeerSecurity(SECURE_AUTH_MECH,
                                                    new ajn::DefaultECDHEAuthListener(),
                                                    NULL,
                                                    false,
                                                    pcl));

        if (result) {
            result = SetupClaimable();
            BUSBOYLOG("::SetupBusAttachment -- SetupClaimable <- %d", result);
        }

        if (result) {
            result = SetManifestTemplate();
            BUSBOYLOG("::SetupBusAttachment -- SetManifestTemplate <- %d", result);
        }
    }

    if (result) {
        ajn::SessionOpts
            opts(ajn::SessionOpts::TRAFFIC_MESSAGES, false, ajn::SessionOpts::PROXIMITY_ANY, ajn::TRANSPORT_ANY);

        AJ_RESULT(result, mBusAttachment->BindSessionPort(mSessionPort, opts, *this));
        BUSBOYLOG("::SetupBusAttachment -- mBusAttachment->BindSessionPort <- %d", result);
    }

    if (mSecure) {
        ajn::PermissionConfigurator::ApplicationState state;
        if (ER_OK == mBusAttachment->GetPermissionConfigurator().GetApplicationState(state)) {
            if (ajn::PermissionConfigurator::CLAIMABLE == state) {
                BUSBOYLOG("provider is not claimed.");
            }
        }
    }

    BUSBOYLOG("::SetupBusAttachment <- %d", result);

    return result;
}

bool BusBoy::DefineInterface() {
    BUSBOYLOG("::DefineInterface -> ");

    bool result = mInterface == NULL;

    if (result) {
        AJ_RESULT(result, mBusAttachment->CreateInterfacesFromXml(mInterfaceXML.c_str()));
        BUSBOYLOG("::DefineInterface -- mBusAttachment->CreateInterfacesFromXml <- %d", result);
    }

    BUSBOYLOG("::DefineInterface <- %d", result);

    return result;
}

bool BusBoy::AttachInterface() {
    BUSBOYLOG("::AttachInterface -> ");

    bool result = true;

    if (mInterface == NULL) {
        result = mBusAttachment != NULL
            && mInterfaceName.length() > 0
            && (mInterface = mBusAttachment->GetInterface(mInterfaceName.c_str())) != NULL;
        BUSBOYLOG("::AttachInterface -- mBusAttachment && mInterfaceName.length && mBusAttachment->GetInterface <- %d",
                  result);

        if (result) {
            AJ_RESULT(result, AddInterface(*mInterface, ajn::BusObject::UNANNOUNCED));
            BUSBOYLOG("::AttachInterface -- AddInterface <- %d", result);
        }

        if (result) {
        	const size_t memberCount = mInterface->GetMembers(NULL, 0);
            const ajn::InterfaceDescription::Member** members = new const ajn::InterfaceDescription::Member*[memberCount];
            mInterface->GetMembers(members, memberCount);

            for (size_t j = 0; j < memberCount; ++j)
            {
                const ajn::InterfaceDescription::Member* member = members[j];

                if (member->description.length() > 0)
                {
                    if (ajn::MESSAGE_METHOD_CALL == member->memberType || ajn::MESSAGE_METHOD_RET == member->memberType)
                    {
                        AJ_RESULT(result, AddMethodHandler(member, static_cast< ajn::MessageReceiver::MethodHandler >(&BusBoy::OnAction)));
                    }
                }
            }

            delete[] members;
        }

        if (result) {
            AJ_RESULT(result, mBusAttachment->RegisterBusObject(*this, mSecure));
            BUSBOYLOG("::AttachInterface -- mBusAttachment->RegisterBusObject <- %d", result);
        }
        
        if(result) {
            // Here we force set the announce on the Introspectable interface as a fix for an alljoyn bug: https://jira.allseenalliance.org/browse/ASACORE-1893
            const ajn::InterfaceDescription* introspectIntf = mBusAttachment->GetInterface("org.allseen.Introspectable");
            if (introspectIntf != NULL)
            {
                AJ_RESULT(result, SetAnnounceFlag(introspectIntf));
            } else {
                result = false;
            }
            BUSBOYLOG("::AttachInterface -- SetAnnounceFlag(\"org.allseen.Introspectable\") <- %d", result);
        }
    }

    BUSBOYLOG("::AttachInterface <- %d", result);

    return result;
}

bool BusBoy::SetupAboutObject() {
    BUSBOYLOG("::SetupAboutObject -> ");

    bool result = mAboutObject == NULL;
    BUSBOYLOG("::SetupAboutObject -- mAboutObject <- %d", result);

    if (result) {
        result = (mAboutData = new ajn::AboutData(mLanguage.c_str())) != NULL;
        BUSBOYLOG("::SetupAboutObject -- new ajn::AboutData <- %d", result);
    }

    if (result) {
        AJ_RESULT(result, mAboutData->CreateFromXml(mAboutXML.c_str()));
        BUSBOYLOG("::SetupAboutObject -- mAboutData->CreateFromXml <- %d", result);
    }

    if (result) {
        result = mAboutData->IsValid(mLanguage.c_str()) == QCC_TRUE;
        BUSBOYLOG("::SetupAboutObject -- mAboutData->IsValid <- %d", result);
    }

    if (mAboutIconData.size() > 0) {
        if (result) {
            result = (mAboutIcon = new ajn::AboutIcon()) != NULL;
            BUSBOYLOG("::SetupAboutObject -- new ajn::AboutIcon <- %d", result);
        }

        if (result) {
            AJ_RESULT(result, mAboutIcon->SetContent("image/png", &mAboutIconData.front(), mAboutIconData.size()));
            BUSBOYLOG("::SetupAboutObject -- mAboutIcon->SetContent <- %d", result);
        }

        if (result) {
            result = (mAboutIconObject = new ajn::AboutIconObj(*mBusAttachment, *mAboutIcon)) != NULL;
            BUSBOYLOG("::SetupAboutObject -- new ajn::AboutIconObj <- %d", result);
        }
    }

    if (result) {
        result = (mAboutObject = new ajn::AboutObj(*mBusAttachment)) != NULL;
        BUSBOYLOG("::SetupAboutObject -- new ajn::AboutObj <- %d", result);
    }

    if (result) {
        AJ_RESULT(result, mAboutObject->Announce(mSessionPort, *mAboutData));
        BUSBOYLOG("::SetupAboutObject -- mAboutObject->Announce <- %d", result);
    }

    BUSBOYLOG("::SetupAboutObject <- %d", result);

    return result;
}

// From SessionPortListener
bool BusBoy::AcceptSessionJoiner(ajn::SessionPort sessionPort, const char *joiner, const ajn::SessionOpts &opts) {
    BUSBOYLOG("::AcceptSessionJoiner -> sessionPort = %d, joiner = %s, opts = %s",
              sessionPort,
              joiner,
              opts.ToString().c_str());

    bool result = sessionPort == mSessionPort;

    BUSBOYLOG("::AcceptSessionJoiner <- %d", result);

    return result;
}

void BusBoy::SessionJoined(ajn::SessionPort sessionPort, ajn::SessionId id, const char *joiner) {
    BUSBOYLOG("::SessionJoined -> sessionPort = %d, id = %d, joiner = %s", sessionPort, id, joiner);
}

void BusBoy::OnAction(const ajn::InterfaceDescription::Member* member, ajn::Message& message) {
	BUSBOYLOG("::OnAction -> action = %s", member->name.c_str());
	DoAction(member, message);
}

QStatus BusBoy::Get(const char* ifcName, const char* propName, ajn::MsgArg& val) {
	BUSBOYLOG("::Get -> property = %s", propName);
	return GetProperty(propName, val) ? ER_OK : ER_BUS_NO_SUCH_PROPERTY;
}

QStatus BusBoy::Set(const char* ifcName, const char* propName, ajn::MsgArg& val) {
	BUSBOYLOG("::Set -> property = %s", propName);
	return SetProperty(propName, val) ? ER_OK : ER_BUS_NO_SUCH_PROPERTY;
}

void BusBoy::PolicyChanged() {

    ajn::PermissionConfigurator::ApplicationState appState;
    if (ER_OK == mBusAttachment->GetPermissionConfigurator().GetApplicationState(appState)) {
        if (ajn::PermissionConfigurator::ApplicationState::CLAIMED == appState) {
//            qcc::Sleep(250); // Allow SecurityMgmtObj to send method reply (see ASACORE-2331)

            QStatus status;
            if (ER_OK != (status = mBusAttachment->SecureConnectionAsync(NULL, true))) {
                BUSBOYLOG("Attempt to secure the connection - status (%s)", QCC_StatusText(status));
            }
        }
    }

}

bool BusBoy::SetupClaimable() {
    bool result = false;

    QStatus status = mBusAttachment->GetPermissionConfigurator().SetClaimCapabilities(
        ajn::PermissionConfigurator::CAPABLE_ECDHE_PSK |
        ajn::PermissionConfigurator::CAPABLE_ECDHE_NULL
    );
    if (status != ER_OK) {
        BUSBOYLOG("Failed to SetClaimCapabilities - status(%s)\n", QCC_StatusText(status));
    } else {

        status = mBusAttachment->GetPermissionConfigurator().SetClaimCapabilityAdditionalInfo(
            ajn::PermissionConfigurator::PSK_GENERATED_BY_SECURITY_MANAGER |
                ajn::PermissionConfigurator::PSK_GENERATED_BY_APPLICATION);

        if (status != ER_OK) {
            BUSBOYLOG("Failed to SetClaimCapabilityAdditionalInfo - status(%s)\n", QCC_StatusText(status));
        } else {
            result = true;
        }
    }

    return result;
}

bool BusBoy::SetManifestTemplate() {
    bool result = false;

    ajn::PermissionPolicy::Rule::Member members[1];
    members[0].SetMemberName("*");
    members[0].SetMemberType(ajn::PermissionPolicy::Rule::Member::NOT_SPECIFIED);
    members[0].SetActionMask(ajn::PermissionPolicy::Rule::Member::ACTION_PROVIDE | ajn::PermissionPolicy::Rule::Member::ACTION_MODIFY | ajn::PermissionPolicy::Rule::Member::ACTION_OBSERVE);

    const size_t manifestSize = 1;
    ajn::PermissionPolicy::Rule manifestTemplate[manifestSize];
    manifestTemplate[0].SetObjPath("*");
    manifestTemplate[0].SetInterfaceName(qcc::String(mInterfaceName.c_str()));
    manifestTemplate[0].SetMembers(1, members);

    AJ_RESULT(result, mBusAttachment->GetPermissionConfigurator().SetPermissionManifest(manifestTemplate, manifestSize));

    return result;
}

} // namespace twobulls

#undef AJ_RESULT
