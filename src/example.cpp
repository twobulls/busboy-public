// Copyright 2015 Two Bulls Holding Pty Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <string>
#include <vector>
#include <iostream>

#include "BusBoy.h"

// Here we inherit from BusBoy because we specifically want to implement an Action handler for our "Press" Action.
//  If we didn't have any Actions to handle, then we could instead just create an instance of higgns::BusBoy directly.
class ExampleBusBoy : 
    public twobulls::BusBoy 
{
	private:
		static const char* LastMessageProperty;
		static const char* SayMessageAction;
		static const char* MessageSaidEvent;

    public:
        // Essentially a passthrough constructor in this case.
        ExampleBusBoy(const std::string& aboutXML, const std::string& interfaceXML, const std::string& pathName,
                	  const ajn::SessionPort port, const std::vector< unsigned char >& iconData = std::vector< unsigned char >()) :
            twobulls::BusBoy(aboutXML, interfaceXML, pathName, port, iconData)
        {};

        // Our Action handler which we register for the "Press" Action we define further down. In this particular case we actually
        //  use the "Press" Action to Trigger the "Pressed" Event. However we could just as easily do something different like;
        //  turn on a device light, or emit a sound, or some other device specific functionality.
        void DoAction(const ajn::InterfaceDescription::Member* member, ajn::Message& message) {
            if (std::string(member->name.c_str()).compare(SayMessageAction) == 0) {
            	if (SetProperty(LastMessageProperty, *(message->GetArg(0)))) {
	            	std::cout << "Message: " << last_message_ << std::endl;
	            	TriggerEvent(MessageSaidEvent);
	            }
            }
        }

        bool GetProperty(const std::string &propertyName, ajn::MsgArg &outPropertyValue) {
        	if (propertyName.compare(LastMessageProperty) == 0) {
        		outPropertyValue.Set("s", last_message_.c_str());
        		return true;
        	}
        	return false;
        }

        bool SetProperty(const std::string &propertyName, const ajn::MsgArg &inPropertyValue) {
            if (propertyName.compare(LastMessageProperty) == 0) {
            	if (inPropertyValue.HasSignature("s")) {
            		char* message = NULL;
            		QStatus status = inPropertyValue.Get("s", &message);
            		if (ER_OK == status) {
            			last_message_ = message;
            			return true;
            		}
            	}
        	}
        	return false;
        }

    private:
    	std::string last_message_;
};

const char* ExampleBusBoy::LastMessageProperty = "LastMessage";
const char* ExampleBusBoy::SayMessageAction = "SayMessage";
const char* ExampleBusBoy::MessageSaidEvent = "MessageSaid";

int main(int argc, char** argv)
{
    // Set AllJoyn logging; can be useful if things aren't working as expected
    //QCC_SetLogLevels("ALLJOYN=7;ALL=7");
    //QCC_UseOSLogging(true);

    // Here we build up an xml metadata description of our Device + Service
	// Differentiate device instances with a unique Id per BusBoy instance
	// DefaultLanguage is an IETF (RFC 5646) language tag
    const char aboutXML[1024] = {
			"<About>"                                           \
        "  <AppId>26123717-c00b-414a-a34f-d96b04260e56</AppId>" \
        "  <DeviceId>26abc717-c00b-414a-a34f-d96b04260e56</DeviceId>"                             \
        "  <AppName>BusBoyMessager</AppName>"                    \
        "  <Manufacturer>Two Bulls</Manufacturer>"              \
        "  <ModelNumber>001</ModelNumber>"                      \
        "  <Description>A device that can receive a message</Description>"   \
        "  <SoftwareVersion>0.0.1</SoftwareVersion>"            \
        "  <DefaultLanguage>en</DefaultLanguage>" \
        "  <DeviceName>BusBoy</DeviceName>"                    \
        "</About>" \
	};

    const char interfaceXML[1024] = {
		"<interface name='com.twobulls.busboy.example'>" \
		"	<description language='en'>This is an example interface</description>" \
		"	<property name='LastMessage' type='s' access='readwrite'>" \
		"		<description>This is an example Property</description>" \
		"	</property>" \
		"	<signal name='MessageSaid' sessionless='true'>" \
		"		<description>This is an example Event</description>" \
		"	</signal>" \
		"	<method name='SayMessage'>" \
		"		<description>This is an example Action</description>" \
		"		<arg name='messageArg' type='s' direction='in'>" \
		"			<description>This is an example Action string argument</description>" \
		"		</arg>" \
		"	</method>" \
		"</interface>" \
    };

    // Use customized BusBoy to take care of boilerplate and setup a BusObject on a BusAttachment
    // running on its own Router (aka Daemon) with included functionality
    ExampleBusBoy busObject(
        aboutXML
        ,interfaceXML
        ,"/com/twobulls/busboy/example"
        ,1337
    );

    // Kick off the BusObject, this does all the required AllJoyn initialization and boilerplate to get the
    // About service advertising the provided description along with similarly described events and actions
    busObject.Start();

    std::cout << std::endl << "CTRL+C/BREAK to exit." << std::endl;
    while (std::cin.ignore()) {
    	// Swallow input until the user CTRL+C/BREAK
    }

    // Teardown the AllJoyn state associated with this BusObject, effectively removing it from the AllJoyn network
    busObject.Stop();

    return 0;
}
