#ifndef TWOBULLS_AJINIT_H
#define TWOBULLS_AJINIT_H

namespace twobulls
{
    
class AJInitGuard
{
public:
    AJInitGuard();
    virtual ~AJInitGuard();
    
    bool isInited();
    
private:
    bool inited_;
};
    
} // namespace twobulls

#endif // TWOBULLS_AJINIT_H
