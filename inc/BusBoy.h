// Copyright 2015 Two Bulls Holding Pty Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef TWOBULLS_BUSBOY_H
#define TWOBULLS_BUSBOY_H

#include <string>
#include <vector>

#include <alljoyn/BusObject.h>
#include <alljoyn/MessageReceiver.h>
#include <alljoyn/SessionPortListener.h>
#include <alljoyn/PermissionConfigurationListener.h>

// Enable Logging
//#define ENABLE_BUSBOY_LOGGING

//#define ENABLE_BUSBOY_BOOST_SHAREDPTR

#if defined(ENABLE_BUSBOY_BOOST_SHAREDPTR)
#include <boost/shared_ptr.hpp>
#include <boost/weak_ptr.hpp>
#endif

#include <alljoyn/KeyStoreListener.h>

#include "AJInit.h"

// Forward Declarations
namespace ajn {
class AboutData;
class AboutIcon;
class AboutIconObj;
class AboutObj;
class BusAttachment;
class InterfaceDescription;
}

namespace twobulls {

// A simplified AllJoyn BusObject that takes care of initializing AllJoyn, registering appropriate interfaces, starting
//  appropriate processes, and announcing to the wider network its presence. It also provides a facility for triggering
//  Events and handling Actions.
//
// The simplifications involve a few limitations that a more direct usage of the AllJoyn API would unlock:
//	* The BusObject pathname is intrinsincally linked to the interface name and port number
//	* The DefaultLanguage is the only language that the BusObject will advertise
//
// There are two main usages:
//	1. No Actions: Instantiate a higgns::BusBoy with appropriate parameters and TriggerEvents on that instance
//	 as required
//	2. With Actions: Define a custom class that inherits from higgns::BusBoy. This class will have extra member
//	 functions that implement the custom behaviour of the defined Actions. Instantiate the custom class with appropriate
//	 parameters and TriggerEvents on that instance as required
//
class BusBoy:
    public ajn::BusObject, public ajn::SessionPortListener, public ajn::PermissionConfigurationListener {
public:

#if defined(ENABLE_BUSBOY_BOOST_SHAREDPTR)
    typedef ::boost::shared_ptr<ajn::BusAttachment> BusAttachmentPtr;
    typedef ::boost::weak_ptr<ajn::BusAttachment> BusAttachmentWeakPtr;
#else
    typedef ajn::BusAttachment* BusAttachmentPtr;
    typedef BusAttachmentPtr BusAttachmentWeakPtr;
#endif

    // The constructor requires some configuration data to appropriately initialize and announce a custom service.
    // 'aboutXML' string is the metadata description of the service as per:
    //	 https://allseenalliance.org/developers/learn/core/about-announcement/interface
    //	 Pay particular attention to the mandatory fields. Also note, that the DefaultLanguage field entry is used
    //	 as the localization tag in all other parts of the advertised interfaces of the BusObject.
    // 'pathName' string is a forward-slash delimited unique identifier for the particular BusObject providing a service.
    //	 eg. /com/company/interface/device
    // 'port' is a unique integer address for differentiating contact instances to the BusObject providing a service.
    //	 eg. 123
    // 'interfaceXML' allows specifying a more flexible interfaceXML in accordance with the AllJoyn schema documented
    //   at https://allseenalliance.org/framework/documentation/develop/api-guide/events-and-actions
    // 'iconData' vector is a byte array containing the png encoded image content used to visually identify this BusObject,
    //	 peers may display this image in association with their representation of the BusObject to the user.
    BusBoy(const std::string &aboutXML,
    	   const std::string &interfaceXML,
    	   const ajn::SessionPort port,
    	   const std::vector<unsigned char> &iconData = std::vector<unsigned char>(),
    	   bool secure = false,
    	   ajn::KeyStoreListener* keyStoreListener = NULL);

    virtual ~BusBoy();

    // This does the bulk of the AllJoyn setup, calling this method should result in a new device announcing
    //	its presence to the wider network.
    // Returns true on success and false on failure, in conjunction with ENABLE_BUSBOY_LOGGING; errors can
    //	be tracked down to particular configuration issues.
    virtual bool Start();

    // This does the teardown of AllJoyn, calling this method should result in the new device no longer being
    //	accessible.
    virtual void Stop();

    // This causes the named Event to be triggered by the BusBoy, this notifies all listeners in the
    //	wider network to such an Event to hear this Event and do things.
    virtual bool TriggerEvent(const std::string &eventName);

    virtual void DoAction(const ajn::InterfaceDescription::Member* member, ajn::Message& message);

	virtual bool GetProperty(const std::string &propertyName, ajn::MsgArg &outPropertyValue);

    virtual bool SetProperty(const std::string &propertyName, const ajn::MsgArg &inPropertyValue);

    // This allows access to the underlying bus attachment, this is useful if you need to send messages or
    //  other actions not wrapped by busboy.
    BusAttachmentWeakPtr GetBusAttachment() { return mBusAttachment; }

    std::string GetInterfaceName() const {
        return mInterfaceName;
    }

    ajn::SessionPort GetSessionPort() const {
        return mSessionPort;
    }

protected:
    // The protected members deal with the intricacies of setting up a valid AllJoyn BusObject, looking into
    //	the source, you can see the order of calls and what information is required.

    bool SetupBusAttachment();
    bool DefineInterface();
    bool AttachInterface();
    bool SetupAboutObject();

    // From SessionPortListener
    bool AcceptSessionJoiner(ajn::SessionPort sessionPort, const char *joiner, const ajn::SessionOpts &opts);
    void SessionJoined(ajn::SessionPort sessionPort, ajn::SessionId id, const char *joiner);

    // PermissionConfigurationListener
    virtual QStatus FactoryReset()
    {
        return ER_NOT_IMPLEMENTED;
    }

    virtual void PolicyChanged();

    void OnAction(const ajn::InterfaceDescription::Member* member, ajn::Message& message);
    QStatus Get(const char* ifcName, const char* propName, ajn::MsgArg& val);
    QStatus Set(const char* ifcName, const char* propName, ajn::MsgArg& val);

    std::string mAboutXML;
    std::string mInterfaceXML;
    std::string mInterfaceName;
    std::vector<unsigned char> mAboutIconData;
    BusAttachmentPtr mBusAttachment;
    ajn::AboutData *mAboutData;
    ajn::AboutObj *mAboutObject;
    ajn::AboutIcon *mAboutIcon;
    ajn::AboutIconObj *mAboutIconObject;
    const ajn::InterfaceDescription *mInterface;
    std::string mApplicationName;
    std::string mLanguage;
    ajn::SessionPort mSessionPort;
    twobulls::AJInitGuard mAJiniter;
    bool mSecure;
    ajn::KeyStoreListener* mKeyStoreListener;

private:
    // The private members are internal methods for grabbing further configuration detail from the provided configuration.

    std::string ExtractInterfaceName(const std::string &interfaceXML);
    std::string ExtractPathName(const std::string &interfaceXML);
    bool DigestAboutXML();
    bool SetupClaimable();
    bool SetManifestTemplate();
};

} // namespace twobulls

#endif // TWOBULLS_BUSBOY_H
