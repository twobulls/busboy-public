BusBoy
======


Dependencies
============

1. A platform appropriate download of the AllJoyn SDK from https://allseenalliance.org/developers/download

2. Build process appropriate to your platform that can include/link the AllJoyn headers/libs


Usage
=====

After setting up the build appropriately, you can copy/paste the BusBoy.h/cpp, AJInit.h/.cpp, tinyxml2.h/.cpp into a larger project,
or simply start hacking away at the example.cpp to play around with different Events and Actions.


License
=======

	Copyright 2015 Two Bulls Holding Pty Ltd

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
